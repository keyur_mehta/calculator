// Ionic Calculator App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('main', {
                url: '/main',
                templateUrl: 'templates/main.html',
                controller: 'mainController'
            });
        $urlRouterProvider.otherwise('/main');
})


.controller('mainController', function ($scope) {
	 
  	$scope.output = "0";
	var accumulate = 0;
	var newNumFlag = false;
	var opPending = "";
	
	$scope.updateOutput = function (Num) {
		
		if (newNumFlag) {
			$scope.output  = Num;
			newNumFlag = false;
		   }
		else {
			if ($scope.output == "0")
			$scope.output = Num;
			else
			$scope.output += Num;
		   }
	}
	
	$scope.calculate = function(Op) {
		var read = $scope.output;
		if (newNumFlag && opPending != "=");
		else
		{
			newNumFlag = true;
			if ( '+' == opPending )
				accumulate += parseFloat(read);
			else if ( '-' == opPending )
				accumulate -= parseFloat(read);
			else if ( '/' == opPending )
				accumulate /= parseFloat(read);
			else if ( '*' == opPending )
				accumulate *= parseFloat(read);
			else
				accumulate = parseFloat(read);
			$scope.output = accumulate;
			opPending = Op;
		}
	}
	
	$scope.reset = function () {
		accumulate = 0;
		opPending = "";
		$scope.output = "0";
		newNumFlag = true;
	}
	
});



